<?php

use Illuminate\Support\Facades\Route;
use Wcby\Cart\Http\Controllers\API\CartController;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api/cart',
], function ($route) {
    $route->get('{uuid}', [CartController::class, 'getCart']);
    $route->post('add/items', [CartController::class, 'addOrUpdateCart']);
    $route->delete('{uuid}/{product_id}', [CartController::class, 'deleteItemCart']);
    $route->delete('{uuid}', [CartController::class, 'deleteCart']);
});

