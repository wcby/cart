<?php

namespace Wcby\Cart\Repositories;

use Wcby\Cart\Models\Cart;
use Ramsey\Uuid;
use Shared\Repositories\SharedRepository;

class CartRepository extends SharedRepository
{
    public function __construct(Cart $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}
