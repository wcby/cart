<?php

namespace Wcby\Cart\Repositories;

use Wcby\Cart\Models\CartItem;
use Shared\Repositories\SharedRepository;

class CartItemRepository extends SharedRepository
{
    public function __construct(CartItem $model)
    {
        $this->model = $model;
        parent::__construct();
    }
}