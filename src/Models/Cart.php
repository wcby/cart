<?php

namespace Wcby\Cart\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'uuid'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function cartItems(): HasMany
    {
        return $this->hasMany(CartItem::class);
    }

    public function user():MorphOne
    {
        return $this->morphOne(User::class, 'cartable');
    }
}
