<?php
namespace Wcby\Cart\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Shared\Models\Products\Product;

class CartItem extends Model
{
    protected $fillable = ['cart_id', 'product_id', 'quantity'];
    protected $hidden = ['created_at', 'updated_at',];
    protected $with = ['product'];

    public function product():BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function cart():BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }
}
