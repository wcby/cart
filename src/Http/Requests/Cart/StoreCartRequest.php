<?php


namespace Amigoshop\Cart\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;

class StoreCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'uuid' => 'required|uuid',
            'product_id' => 'required|integer|exists:products,id',
            'quantity' => 'required|integer',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'uuid.uuid' => 'Uuid должен быть строкой',
            'uuid.required' => 'Укажите uuid',
            'quantity.required' => 'Укажите количество товара',
            'quantity.integer' => 'Количество товара должно быть числовом',
            'product_id.integer' => 'Ид товара должен быть числовым',
            'product_id.exists' => 'Указанного товара не существует'
        ];
    }
}

