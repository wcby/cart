<?php


namespace Amigoshop\Cart\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;

class CartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'uuid' => 'required|uuid',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'uuid.uuid' => 'Текст должен быть строкой',
            'uuid.required' => 'Укажите оценку',
        ];
    }
}

