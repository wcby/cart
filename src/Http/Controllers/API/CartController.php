<?php

namespace Wcby\Cart\Http\Controllers\API;
use Wcby\Cart\Http\Controllers\Controller;
use Wcby\Cart\Http\Requests\Cart\StoreCartRequest;
use Wcby\Cart\Services\CartService;
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use Illuminate\Http\JsonResponse;

class CartController extends Controller
{
    public function __construct(protected CartService $cartService, public  ResponseContract $json)
    {
        parent::__construct($json);
    }

    /**
     * @OA\Get(
     *      path="/cart/{uuid}",
     *      operationId="cart",
     *      tags={"Корзина"},
     *      summary="Получить корзину",
     *      description="Получить корзину",
     *      @OA\Parameter(name="uuid", required=true, in="path",
     *      @OA\Schema(type="string", format="uuid"), example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *      @OA\Response(
     *          response=200,
     *          description="Успешный ответ",
     *              @OA\JsonContent(
     *                  @OA\Property(
     *                      property="cart",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", example=2, description="ID корзины"),
     *                          @OA\Property(property="uuid", type="uuid", example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *                          @OA\Property(property="total", type="integer", example="6"),
     *                          @OA\Property(
     *                              property="cartItem",
     *                              type="array",
     *                              @OA\Items(
     *                                  @OA\Property(property="cart_id", type="integer", example=2, description="ID корзины"),
     *                                  @OA\Property(property="quantity", type="integer", example=2, description="Количество товара"),
     *                                  @OA\Property(
     *                                      property="cartItem",
     *                                      type="array",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", example="1", description="id товара"),
     *                                          @OA\Property(property="name", type="string", example="Iphone 13", description="Количество товара"),
     *                                          @OA\Property(property="short_description", type="string", example="<p>краткое описание</p>", description="Краткое описание товара"),
     *                                          @OA\Property(property="slug", type="string", example="nazvanie-tovara", description="Url товара"),
     *                                          @OA\Property(property="price", type="object",
     *                                              @OA\Property(property="current", type="integer", example=800, description="Цена за единицу товара"),
     *                                              @OA\Property(property="old", type="integer", example=1200, description="Старая цена за единицу товара"),
     *                                          ),
     *                                          @OA\Property(property="images", type="array",
     *                                              @OA\Items(
     *                                                  @OA\Property(property="id", type="integer", example="1", description="id картинки"),
     *                                                  @OA\Property(property="product_id", type="integer", example="2", description="Id продукта"),
     *                                                  @OA\Property(property="main", type="string", example="/storage/images/main/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке main"),
     *                                                  @OA\Property(property="image", type="object",
     *                                                      @OA\Property(property="original", type="string", example="/storage/images/original/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке original"),
     *                                                      @OA\Property(property="card", type="string", example="/storage/images/card/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке card"),
     *                                                      @OA\Property(property="snippet", type="string", example="/storage/images/snippet/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке snippet"),
     *                                                      @OA\Property(property="slider", type="string", example="/storage/images/slider/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке slider"),
     *                                                  ),
     *                                                  @OA\Property(property="sort", type="integer", example="0", description="Номер (позиция) картинки"),
     *                                                  @OA\Property(property="created_at", type="integer", example="1685007838", description="Дата создания картиник"),
     *                                                  @OA\Property(property="updated_at", type="integer", example="1685007838", description="Дата обновления картиник"),
     *                                              ),
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *       ),
     * )
     */
    public function getCart($uuid):JsonResponse
    {
        $cartData = $this->cartService->getCartData($uuid);
        return $this->successResponse(['cart' => $cartData]);
    }

    /**
     * @OA\Post(
     *      path="/cart/add/items",
     *      operationId="cartAdd",
     *      tags={"Корзина"},
     *      summary="Добавить/обновить",
     *      description="Добавить или обновить",
     *      security={{"cart":{}}},
     *      @OA\Parameter(name="uuid", required=true, in="query",
     *      @OA\Schema(type="string", format="uuid"), example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *      @OA\Parameter(name="product_id", required=true, in="query",
     *      @OA\Schema(type="integer"), example="3", description="id товара"),
     *      @OA\Parameter(name="quantity", required=true, in="query",
     *      @OA\Schema(type="integer"), example="2", description="Кол-во товара"),
     *      @OA\Response(
     *          response=200,
     *          description="Успешный ответ",
     *              @OA\JsonContent(
     *                  @OA\Property(
     *                      property="cart",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", example=2, description="ID корзины"),
     *                          @OA\Property(property="uuid", type="uuid", example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *                          @OA\Property(property="total", type="integer", example="6"),
     *                          @OA\Property(
     *                              property="cartItem",
     *                              type="array",
     *                              @OA\Items(
     *                                  @OA\Property(property="cart_id", type="integer", example=2, description="ID корзины"),
     *                                  @OA\Property(property="quantity", type="integer", example=2, description="Количество товара"),
     *                                  @OA\Property(
     *                                      property="cartItem",
     *                                      type="array",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", example="1", description="id товара"),
     *                                          @OA\Property(property="name", type="string", example="Iphone 13", description="Количество товара"),
     *                                          @OA\Property(property="short_description", type="string", example="<p>краткое описание</p>", description="Краткое описание товара"),
     *                                          @OA\Property(property="slug", type="string", example="nazvanie-tovara", description="Url товара"),
     *                                          @OA\Property(property="price", type="object",
     *                                              @OA\Property(property="current", type="integer", example=800, description="Цена за единицу товара"),
     *                                              @OA\Property(property="old", type="integer", example=1200, description="Старая цена за единицу товара"),
     *                                          ),
     *                                          @OA\Property(property="images", type="array",
     *                                              @OA\Items(
     *                                                  @OA\Property(property="id", type="integer", example="1", description="id картинки"),
     *                                                  @OA\Property(property="product_id", type="integer", example="2", description="Id продукта"),
     *                                                  @OA\Property(property="main", type="string", example="/storage/images/main/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке main"),
     *                                                  @OA\Property(property="image", type="object",
     *                                                      @OA\Property(property="original", type="string", example="/storage/images/original/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке original"),
     *                                                      @OA\Property(property="card", type="string", example="/storage/images/card/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке card"),
     *                                                      @OA\Property(property="snippet", type="string", example="/storage/images/snippet/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке snippet"),
     *                                                      @OA\Property(property="slider", type="string", example="/storage/images/slider/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке slider"),
     *                                                  ),
     *                                                  @OA\Property(property="sort", type="integer", example="0", description="Номер (позиция) картинки"),
     *                                                  @OA\Property(property="created_at", type="integer", example="1685007838", description="Дата создания картиник"),
     *                                                  @OA\Property(property="updated_at", type="integer", example="1685007838", description="Дата обновления картиник"),
     *                                              ),
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *       ),
     * )
     */
    public function addOrUpdateCart(StoreCartRequest $request):JsonResponse
    {
        $requestData = $request->only(['uuid', 'product_id', 'quantity']);

        $cartData = $this->cartService->addOrUpdateCart($requestData);
        return $this->successResponse(['cart' => $cartData]);
    }
    /**
     * @OA\Delete(
     *      path="/cart/{uuid}/{product_id}",
     *      operationId="cartDeleteItem",
     *      tags={"Корзина"},
     *      summary="Удаление позиии",
     *      description="Удаление товара из корзины",
     *      security={{"cart":{}}},
     *      @OA\Parameter(name="uuid", required=true, in="path",
     *      @OA\Schema(type="string", format="uuid"), example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *      @OA\Parameter(name="product_id", required=true, in="path",
     *      @OA\Schema(type="integer"), example="2", description="id товара"),
     *      @OA\Response(
     *          response=200,
     *          description="Успешный ответ",
     *              @OA\JsonContent(
     *                  @OA\Property(
     *                      property="cart",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", example=2, description="ID корзины"),
     *                          @OA\Property(property="uuid", type="uuid", example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *                          @OA\Property(property="total", type="integer", example="6"),
     *                          @OA\Property(
     *                              property="cartItem",
     *                              type="array",
     *                              @OA\Items(
     *                                  @OA\Property(property="cart_id", type="integer", example=2, description="ID корзины"),
     *                                  @OA\Property(property="quantity", type="integer", example=2, description="Количество товара"),
     *                                  @OA\Property(
     *                                      property="cartItem",
     *                                      type="array",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", example="1", description="id товара"),
     *                                          @OA\Property(property="name", type="string", example="Iphone 13", description="Количество товара"),
     *                                          @OA\Property(property="short_description", type="string", example="<p>краткое описание</p>", description="Краткое описание товара"),
     *                                          @OA\Property(property="slug", type="string", example="nazvanie-tovara", description="Url товара"),
     *                                          @OA\Property(property="price", type="object",
     *                                              @OA\Property(property="current", type="integer", example=800, description="Цена за единицу товара"),
     *                                              @OA\Property(property="old", type="integer", example=1200, description="Старая цена за единицу товара"),
     *                                          ),
     *                                          @OA\Property(property="images", type="array",
     *                                              @OA\Items(
     *                                                  @OA\Property(property="id", type="integer", example="1", description="id картинки"),
     *                                                  @OA\Property(property="product_id", type="integer", example="2", description="Id продукта"),
     *                                                  @OA\Property(property="main", type="string", example="/storage/images/main/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке main"),
     *                                                  @OA\Property(property="image", type="object",
     *                                                      @OA\Property(property="original", type="string", example="/storage/images/original/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке original"),
     *                                                      @OA\Property(property="card", type="string", example="/storage/images/card/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке card"),
     *                                                      @OA\Property(property="snippet", type="string", example="/storage/images/snippet/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке snippet"),
     *                                                      @OA\Property(property="slider", type="string", example="/storage/images/slider/Pl04r2Ew6c7Vj7A7.jpg", description="Путь к картинке slider"),
     *                                                  ),
     *                                                  @OA\Property(property="sort", type="integer", example="0", description="Номер (позиция) картинки"),
     *                                                  @OA\Property(property="created_at", type="integer", example="1685007838", description="Дата создания картиник"),
     *                                                  @OA\Property(property="updated_at", type="integer", example="1685007838", description="Дата обновления картиник"),
     *                                              ),
     *                                          ),
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *       ),
     * )
     */
    public function deleteItemCart($uuid, $product_id):JsonResponse
    {
        $cartData = $this->cartService->deleteCartItem($uuid, $product_id);
        return $this->successResponse(['cart' => $cartData]);
    }
    /**
     * @OA\Delete(
     *      path="/cart/{uuid}",
     *      operationId="cartDelete",
     *      tags={"Корзина"},
     *      summary="Очистить корзину",
     *      description="Очистить корзину",
     *      security={{"cart":{}}},
     *      @OA\Parameter(name="uuid", required=true, in="path",
     *      @OA\Schema(type="string", format="uuid"), example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *      @OA\Response(
     *          response=200,
     *          description="Успешный ответ",
     *              @OA\JsonContent(
     *                  @OA\Property(
     *                      property="cart",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", example=2, description="ID корзины"),
     *                          @OA\Property(property="uuid", type="uuid", example="9675e665-1923-4c6f-97c0-259b191fab24", description="uuid"),
     *                          @OA\Property(property="total", type="integer", example="0"),
     *                      )
     *                  )
     *          )
     *       ),
     * )
     */
    public function deleteCart($uuid):JsonResponse
    {
        $cartData = $this->cartService->deleteCart($uuid);
        if($cartData){
            return $this->successResponse(['cart' => $cartData]);
        }
        return $this->failResponse(['cart' => $cartData], 400, 'Error', ['Такого uuid не существует']);
    }
}
