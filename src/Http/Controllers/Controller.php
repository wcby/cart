<?php

namespace Wcby\Cart\Http\Controllers;

use App\Http\Controllers\Controller as BackendController;

class Controller extends BackendController
{
    public function response($data, $message = 'Success', $errors = null)
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'errors' => $errors,
        ]);
    }

}
