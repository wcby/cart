<?php

namespace Wcby\Cart\Services;

use Wcby\Cart\Repositories\CartItemRepository;
use Wcby\Cart\Repositories\CartRepository;
use Wcby\Cart\Traits\ProductTrait;

class CartService
{
    use ProductTrait;

    public function __construct(
        public CartRepository $cartRepository,
        public CartItemRepository $cartItemRepository)
    {
    }

    public function getCartData(string $uuid):array
    {
        $cart = $this->cartRepository->firstByFilters(['uuid'=>$uuid], ['cartItems.product']);
        if(!$cart){
            $cart = $this->addCart($uuid);
        }
        return $this->getCartDetails($cart);
    }

    public function addCart($uuid)
    {
        return $this->cartRepository->create(
            $this->cartRepository->getNewModelInstance(['uuid' => $uuid])
        );
    }

    public function addOrUpdateCart(array $request): array
    {
        $cart = $this->cartRepository->firstByFilters(['uuid' => $request['uuid']], ['cartItems.product']);
        if (!$cart) {
            $cart = $this->addCart($request['uuid']);
        }
        return $this->updateCartItem($cart, $request);
    }

    private function updateCartItem($cart, array $requestData): array
    {
        if ($cart->cartItems && !$cart->cartItems->isEmpty()) {
            $itemCart = $cart->cartItems->firstWhere('product_id', $requestData['product_id']);

            if ($itemCart) {
                $itemCart->quantity = (int)$requestData['quantity'];
                $itemCart->save();
            } else {
                $this->createCartItem($cart, $requestData);
            }
        } else {
            $this->createCartItem($cart, $requestData);
        }
        // Обновление данных корзины
        $cart->refresh();

        // Получение детальной информации о корзине
        return $this->getCartDetails($cart);
    }

    private function createCartItem($cart, array $requestData): void
    {
        $this->cartItemRepository->create(
            $this->cartItemRepository->getNewModelInstance(
                [
                    'cart_id' => $cart->id,
                    'product_id' => $requestData['product_id'],
                    'quantity' => (int)$requestData['quantity'],
                ])
        );
    }


    public function deleteCartItem(string $uuid, int $product_id):array
    {
        $cart = $this->cartRepository->firstByFilters(['uuid'=>$uuid], ['cartItems.product']);

        if ($cart) {
            $itemDelete = $cart->cartItems->firstWhere('product_id', $product_id);
            if ($itemDelete) {
                $key = $cart->cartItems->search($itemDelete);
                if ($key !== false) {
                    $cart->cartItems->forget($key);
                }
            }
            $cartItem = $this->cartItemRepository->firstByFilters([
                'product_id' => $product_id,
                'cart_id' => $cart['id']
            ]);
            if ($cartItem) {
                $cartItem->delete();
            }
        }
        return  $this->getCartDetails($cart);
    }

    public function deleteCart(string $uuid):array
    {
        $cart = $this->cartRepository->firstByFilters(['uuid'=>$uuid], ['cartItems.product']);

        if ($cart) {
            $ids = $cart->cartItems->pluck('id')->toArray();
            $this->cartItemRepository->deleteMany($ids);
            $cart->delete();
            $cartData = [];
        }else{
            $cartData = $this->getCartDetails($cart);
        }

        return $cartData;
    }

    public function getCartDetails($cart): array
    {
        $cartItems = []; // Инициализировать $cartItems значением null по умолчанию
        $data = [];
        $total = 0;
        if ($cart && $cart->cartItems->count() > 0) { // Проверка на существование $cart и наличие связанных элементов в cartItems
            $cartItems = $cart->cartItems->map(function($cartItem) {
                $price = [
                    'current' => $this->getPrice($cartItem),
                    'old' => $this->getOldPrice($cartItem)
                ];
                $name = $this->getName($cartItem);
                $image = $this->getImageCard($cartItem);
                $images = $this->getImages($cartItem);
                $product = [
                    'id' => $cartItem->product->id,
                    'name' => $cartItem->product->name,
                    'short_description' => $cartItem->product->short_description,
                    'slug' => $cartItem->product->slug,
                    'price' => $price,
                    'images' => $images,
                ];
                return [
                    'cart_id' => $cartItem->cart_id,
                    'quantity' => $cartItem->quantity,
                    'product' => $product,
                ];
            });
            $total = $cartItems->sum('quantity');
            $cartItems = $cartItems->toArray();
        }
        if($cart)
        {
            $data['id'] = $cart->id;
            $data['uuid'] = $cart->uuid;
            $data['total'] = $total;
//            $data['user'] = $cart->cartable; // assuming 'cartable' is associated with 'users' table
            $data['cartItems'] = $cartItems;
        }
        return $data;
    }
}
