<?php

namespace Amigoshop\Cart\Services;

use Amigoshop\Cart\Repositories\CartItemRepository;
use Amigoshop\Cart\Repositories\CartRepository;
use Ramsey\Uuid\Uuid;

class CartUserService
{
    public function __construct(protected CartService $cartService, public CartRepository $cartRepository,public CartItemRepository $cartItemRepository)
    {
    }

    public static function generateUuidFromValue($value): string
    {
        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, (string)$value);
        return $uuid->toString();
    }

    public function getCartAuthUser($userId): array
    {
        $uuidAuthUser = $this->generateUuidFromValue($userId);
        $cartUser = $this->cartRepository->firstByFilters(['uuid' => $uuidAuthUser], ['cartItems.product']);
        return $this->cartService->getCartDetails($cartUser);;
    }

    public function mergeCarts($uuid, $userId): array
    {
        $cartFirst = $this->getCartAuthUser($userId);
        $cartSecond = $this->cartRepository->firstByFilters(['uuid' => $uuid], ['cartItems.product']);

        if (!$cartFirst || !$cartSecond) {
            // Обработка ситуации, если одна из корзин не найдена
            return ['error' => 'Ни одной корзины не найдено'];
        }

        // Собираем все id товаров из первой корзины
        $existingProductIds = $cartFirst->cartItems->pluck('product_id')->toArray();

        // Добавляем позиции из второй корзины, если их там еще нет
        foreach ($cartSecond->cartItems as $cartItem) {
            if (!in_array($cartItem->product_id, $existingProductIds)) {
                $newCartItem = $this->cartItemRepository->create([
                    'cart_id' => $cartFirst->id,
                    'product_id' => $cartItem->product_id,
                    'quantity' => $cartItem->quantity,
                ]);
                $cartFirst->cartItems->push($newCartItem); // Добавляем новую позицию в коллекцию позиций корзины
            }
        }

        // Удаляем вторую корзину
        $this->cartRepository->deleteByFilters(['uuid' => $uuid]);

        return $this->getCartDetails($cartFirst);
    }
}
