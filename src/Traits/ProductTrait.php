<?php

namespace Wcby\Cart\Traits;

trait ProductTrait
{
    public function getPrice($listItem)
    {
        return $listItem->product->price() ?? 0;
    }

    public function getOldPrice($listItem)
    {
        return $listItem->product->oldPrice() ?? 0;
    }

    public function getImageCard($listItem)
    {
        return $listItem->product->images()->first()->image->snippet ?? '';
    }

    public function getImages($listItem)
    {
        return $listItem->product->images ?? [];
    }

    public function getCategory($listItem)
    {
        return $listItem->product->category ?? [];
    }

    public function getName($cartItem)
    {
        return $cartItem->product->name ?? '';
    }
}
