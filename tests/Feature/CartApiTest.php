<?php
namespace Tests\Unit\Services;

use Wcby\Cart\Services\CartService;
use Wcby\Cart\Models\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;

class CartServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testGetCartData()
    {
        $cartService = new CartService();
        $cart = Cart::factory()->create();
        $uuid = $cart->uuid;

        $cartData = $cartService->getCartData($cart, $uuid);

        $this->assertIsArray($cartData);
    }

    public function testAddOrUpdateCartItem()
    {
        $cartService = new CartService();
        $cart = Cart::factory()->create();
        $uuid = $cart->uuid;
        $request = new Request([
            'uuid' => $uuid,
            'product_id' => 1,
            'quantity' => 2,
        ]);

        $cartData = $cartService->addOrUpdateCartItem($request, $cart);

        $this->assertIsArray($cartData);
    }

    public function testDeleteCartItem()
    {
        $cartService = new CartService();
        $cart = Cart::factory()->create();
        $uuid = $cart->uuid;
        $request = new Request([
            'uuid' => $uuid,
            'product_id' => 1,
        ]);

        $cartData = $cartService->deleteCartItem($request, $cart);

        $this->assertIsArray($cartData);
    }

    public function testDeleteCart()
    {
        $cartService = new CartService();
        $cart = Cart::factory()->create();
        $uuid = $cart->uuid;
        $request = new Request(['uuid' => $uuid]);

        $cartData = $cartService->deleteCart($request, $cart);

        $this->assertIsArray($cartData);
    }
}
