<?php

namespace Tests\Unit\Services;

class CartUserServiceTest extends TestCase
{
    public function testGenerateUuidFromValue()
    {
        $uuid = cartUserService::generateUuidFromValue('some_value');
        $this->assertIsString($uuid);
        $this->assertRegExp('/^[a-f0-9]{8}-[a-f0-9]{4}-5[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$/', $uuid);
    }

    public function testGetCartAuthUser()
    {
        // Мокаем зависимость cartService для возврата фиктивных деталей корзины
        $this->cartService->shouldReceive('getCartDetails')->andReturn(['fake_cart_details']);
        $cartDetails = $this->cartUserService->getCartAuthUser();
        $this->assertEquals(['fake_cart_details'], $cartDetails);
    }

    public function testMergeCarts()
    {
        // Мокаем зависимости для возврата фиктивных значений и проверяем логику объединения корзин
        $this->cartService->shouldReceive('getCartDetails')->andReturn(['fake_cart_details']);
        $this->cartRepository->shouldReceive('firstByFilters')->andReturn($fake_cart);
        $this->cartRepository->shouldReceive('deleteByFilters');
        $mergedCart = $this->cartUserService->mergeCarts('fake_uuid');
        $this->assertEquals(['fake_cart_details'], $mergedCart);
    }
}